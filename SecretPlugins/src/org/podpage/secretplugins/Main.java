package org.podpage.secretplugins;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.spigotmc.SpigotConfig;

/*
 * @author podpage
 * Don't steal this code!
 */

public class Main extends JavaPlugin {

	private Logger log;

	@Override
	public void onEnable() {
		log = Logger.getLogger("Minecraft");
		Bukkit.getScheduler().runTaskLater(this, new Runnable() {

			@Override
			public void run() {
				fixCommands();
			}
		}, 10);
	}

	public void fixCommands() {
		if (SpigotConfig.unknownCommandMessage.equals("Unknown command. Type \"/help\" for help.")) {
			SpigotConfig.unknownCommandMessage = "Unknown command. Ask an Admin for help";
		}
		try {
			File f = new File(getServer().getWorldContainer().getAbsolutePath(), "commands.yml");
			if (f.exists()) {
				YamlConfiguration yml = YamlConfiguration.loadConfiguration(f);
				if (yml.contains("aliases.icanhasbukkit")) {
					yml.set("aliases.icanhasbukkit", null);
					yml.save(f);
				}
			}
			CommandMap commandMap = (CommandMap) getValue(Bukkit.getServer(), "commandMap");
			SimpleCommandMap cs = (SimpleCommandMap) commandMap;
			@SuppressWarnings("unchecked")
			Map<String, Command> knownCommands = (Map<String, Command>) getValue(cs, "knownCommands");
			String[] commands = new String[] { "plugins", "pl", "ver", "version", "about", "help", "?" };

			for (String s : commands) {
				knownCommands.remove(s);
				knownCommands.remove("bukkit:" + s);
			}
			log.info("Done! Your Plugins are now hidden");
		} catch (Exception e) {
			e.printStackTrace();
			log.warning("Failed to save file [commands.yml]");
			log.info("Please report this bug - Twitter @podpage");
		}
	}

	public Object getValue(Object packet, String fieldname) {
		try {
			Field field = packet.getClass().getDeclaredField(fieldname);
			field.setAccessible(true);
			return field.get(packet);
		} catch (Exception e) {
			e.printStackTrace();
			log.warning("Failed to get field [" + fieldname + "] in class [" + packet.getClass().getSimpleName() + "]");
			log.info("Please report this bug - Twitter @podpage");
		}
		return null;
	}
}